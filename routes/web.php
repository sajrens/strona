<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::resource('pages','PagesController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/kontrahenci', 'HomeController@kontrahenci')->name('kontrahenci');
Route::get('/firma', 'HomeController@firma')->name('firma');
Route::get('/uslugi', 'HomeController@uslugi')->name('uslugi');
Route::get('/firma_create', 'HomeController@firma_create')->name('firma_create');
Route::post('/firma_done', 'HomeController@firma_done');
Route::get('/kontrahenci_dodaj', 'HomeController@kontrahenci_dodaj');
Route::post('/kontrahenci_add', 'HomeController@kontrahenci_add');
Route::post('/kontrahenci_usun', 'HomeController@kontrahenci_usun');
Route::post('/kontrahenci_edytuj', 'HomeController@kontrahenci_edytuj');
Route::post('/kontrahenci_edit', 'HomeController@kontrahenci_edit');
Route::post('/uslugi_usun', 'HomeController@uslugi_usun');
Route::post('/uslugi_edytuj', 'HomeController@uslugi_edytuj');
Route::post('/uslugi_edit', 'HomeController@uslugi_edit');
Route::get('/uslugi_dodaj', 'HomeController@uslugi_dodaj');
Route::post('/uslugi_add', 'HomeController@uslugi_add');
Route::post('/firma_usunpracownika', 'HomeController@firma_usunpracownika');
Route::get('/firma_dodajpracownika', 'HomeController@firma_dodajpracownika');
Route::post('/firma_invitepracownika', 'HomeController@firma_invitepracownika');
Route::get('/pracownik_zaproszenia', 'HomeController@pracownik_zaproszenia');
Route::post('/pracownik_przyjmij', 'HomeController@pracownik_przyjmij');
Route::get('/faktura_dodaj', 'HomeController@faktura_dodaj');
Route::post('/faktura_nowa', 'HomeController@faktura_nowa');
Route::get('/pdf_pobierz', 'HomeController@pdf_pobierz');
Route::post('/faktura_usun', 'HomeController@faktura_usun');
Route::post('/customers/pdf','HomeController@export_pdf');
Route::get('/test', 'HomeController@test');
Route::get('/firma_opusc', 'HomeController@firma_opusc');





