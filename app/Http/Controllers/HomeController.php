<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function kontrahenci()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            return view('kontrahenci');
        }
    }

    public function firma()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            DB::transaction(function () {
            $owner = DB::table('firma')->where('id_firmy', Auth::user()->id_firmy)->value('id_owner');
            });
            if (Auth::user()->id == $owner) {
                return view('firma_owner');
            } else {
                return view('firma_pracownik');
            }
        }

    }

    public function firma_create()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma_create');
        } else {
            return view('firma_owner');
        }

    }

    public function firma_done()
    {
        if (Auth::user()->id_firmy == 0) {
            DB::insert('insert into firma (id_owner,nazwa,ulica,lokal,miasto,NIP,Nr_tel,email,bank_name,bank_number ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [Auth::user()->id, $_POST['nazwa'], $_POST['ulica'], $_POST['lokal'], $_POST['miasto'], $_POST['nip'], $_POST['telefon'], $_POST['email'], $_POST['bank_name'], $_POST['bank_nr']]);


            $name = DB::table('firma')->where('id_owner', Auth::user()->id)->pluck('id_firmy');
            $name = str_replace("[", "", $name);
            $name = str_replace("]", "", $name);
            DB::update('update users set id_firmy = ? where id = ?', [$name, Auth::user()->id]);


            return view('firma_owner');
        } else {
            return view('firma_obsluga');
        }


    }

    public function kontrahenci_dodaj()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            return view('kontrahenci_dodaj');
        }

    }

    public function kontrahenci_add()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            DB::insert('insert into kontrahenci (id_firmy,imie,nazwisko,ulica,lokal,miasto,kod_pocztowy,NIP,Nr_tel,krs,email ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)', [Auth::user()->id_firmy, $_POST['imie'], $_POST['nazwisko'], $_POST['ulica'], $_POST['lokal'], $_POST['miasto'], $_POST['kod_pocztowy'], $_POST['nip'], $_POST['nr_tel'], $_POST['krs'], $_POST['email']]);

            return redirect()->to('/kontrahenci');
        }

    }

    public function kontrahenci_usun()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {

            DB::insert('delete from kontrahenci WHERE id_kontrahenta = ?', [$_POST['id']]);
            unset($_POST);

            return redirect()->to('/kontrahenci');

        }

    }

    public function kontrahenci_edytuj()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            return view('kontrahenci_edytuj');
        }

    }

    public function kontrahenci_edit()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            DB::table('kontrahenci')
                ->where('id_kontrahenta', $_POST['id'])
                ->update(['imie' => $_POST['imie'], 'nazwisko' => $_POST['nazwisko'], 'ulica' => $_POST['ulica'], 'lokal' => $_POST['lokal'], 'miasto' => $_POST['miasto'], 'kod_pocztowy' => $_POST['kod_pocztowy'], 'NIP' => $_POST['nip'], 'Nr_tel' => $_POST['Nr_tel'], 'krs' => $_POST['krs'], 'email' => $_POST['email']]);
            return redirect()->to('/kontrahenci');
        }

    }

    public function uslugi()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            return view('uslugi');
        }

    }

    public function uslugi_usun()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            DB::insert('delete from uslugi WHERE id_uslugi = ?', [$_POST['id']]);
            unset($_POST);

            return redirect()->to('/uslugi');
        }

    }
    public function uslugi_edytuj()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            return view('uslugi_edytuj');
        }

    }
    public function uslugi_edit()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            DB::table('uslugi')
                ->where('id_uslugi', $_POST['id'])
                ->update(['nazwa' => $_POST['nazwa'], 'cena_netto' => $_POST['cena'], 'vat' => $_POST['vat']]);
            return redirect()->to('/uslugi');
        }

    }

    public function uslugi_dodaj()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            return view('uslugi_dodaj');
        }

    }

    public function uslugi_add()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            DB::insert('insert into uslugi (id_firmy,nazwa,cena_netto,vat) values (?, ?, ?, ?)', [Auth::user()->id_firmy, $_POST['nazwa'], $_POST['cena_netto'], $_POST['vat']]);

            return redirect()->to('/uslugi');
        }

    }

    public function firma_usunpracownika()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            DB::update('update users set id_firmy = ? where id = ?', [0, $_POST['id']]);
            unset($_POST);

            return redirect()->to('/firma');
        }

    }

    public function firma_dodajpracownika()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            $owner = DB::table('firma')->where('id_firmy', Auth::user()->id_firmy)->value('id_owner');
            if (Auth::user()->id == $owner) {
                return view('firma_dodajpracownika');
            } else {
                return view('firma_pracownik');
            }
        }

    }

    public function firma_invitepracownika()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            $owner = DB::table('firma')->where('id_firmy', Auth::user()->id_firmy)->value('id_owner');
            if (Auth::user()->id == $owner) {
                DB::insert('insert into zaproszenia (id_firmy,id_user) values (?, ?)', [Auth::user()->id_firmy, $_POST['id']]);
                return redirect()->to('/firma_dodajpracownika');
            } else {
                return view('firma_pracownik');
            }
        }

    }

    public function pracownik_zaproszenia()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('pracownik_zaproszenia');
        } else {
            return redirect()->to('/firma');
        }
    }
    public function pracownik_przyjmij()
    {
        if (Auth::user()->id_firmy == 0) {
            DB::update('update users set id_firmy = ? where id = ?', [$_POST['firma'], $_POST['id']]);
            DB::insert('delete from zaproszenia WHERE id_firmy = ? AND id_user = ?', [$_POST['firma'], $_POST['id']]);
            return redirect()->to('/firma');
        } else
            {
            return redirect()->to('/firma');
        }
    }
    public function faktura_dodaj()
    {
        if (Auth::user()->id_firmy == 0) {
            return redirect()->to('/firma');
        } else {
            return view('faktura_dodaj');
        }
    }
    public function faktura_nowa()
    {
        if (Auth::user()->id_firmy == 0) {
            return redirect()->to('/firma');
        } else
            {
            if(empty($_POST['imie']))
            {
                $iduslug="";
                for($i=1;$i<=$_POST['ilosc_uslug'];$i++)
                {
                    $id_uslugi = explode("|", $_POST["usluga".$i]);
                    $iduslug=$iduslug.$id_uslugi[0].',';

                }


                $kontrahent = explode("|", $_POST["kontrahent"]);
                DB::insert('insert into faktury (id_firmy,id_kontrahenta,id_uslug,data) values (?, ?,?,?)', [Auth::user()->id_firmy,$kontrahent[0],$iduslug,date("m.d.y")]);
                return redirect()->to('/pdf_pobierz');
            }
            else
            {
                DB::insert('insert into kontrahenci (id_firmy,imie,nazwisko,ulica,lokal,miasto,kod_pocztowy,NIP,Nr_tel,krs,email ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)', [Auth::user()->id_firmy, $_POST['imie'], $_POST['nazwisko'], $_POST['ulica'], $_POST['lokal'], $_POST['miasto'], $_POST['kod_pocztowy'], $_POST['nip'], $_POST['nr_tel'], $_POST['krs'], $_POST['email']]);
                $kontrahent = DB::table('kontrahenci')->where('email', $_POST['email'])->pluck('id_kontrahenta');
                $kontrahent = str_replace("[", "", $kontrahent);
                $kontrahent = str_replace("]", "", $kontrahent);
                $iduslug="";
                for($i=1;$i<=$_POST['ilosc_uslug'];$i++)
                {
                    $id_uslugi = explode("|", $_POST["usluga".$i]);
                    $iduslug=$iduslug.$id_uslugi[0].',';

                }



                DB::insert('insert into faktury (id_firmy,id_kontrahenta,id_uslug,data) values (?, ?,?,?)', [Auth::user()->id_firmy,$kontrahent,$iduslug,date("m.d.y")]);
                return redirect()->to('/pdf_pobierz');
            }
        }
    }
    public function pdf_pobierz()
    {
        if (Auth::user()->id_firmy == 0) {
            return redirect()->to('/firma');
        } else {
            return view('pdf_pobierz');
        }
    }
    public function faktura_usun()
    {
        if (Auth::user()->id_firmy == 0) {
            return redirect()->to('/firma');
        } else {
            DB::insert('delete from faktury WHERE id_faktury = ?',[$_POST['id']]);
            return redirect()->to('/pdf_pobierz');
        }
    }
    public function export_pdf()
    {
        if (Auth::user()->id_firmy == 0) {
            return redirect()->to('/firma');
        } else {
            $data[0] = $_POST['id'];
            $data[1] = 2;
            $pdf = PDF::loadView('test',$data);
            return $pdf->download('test.pdf');
        }
    }
    public function test()
    {
        return view('test');

    }
    public function firma_opusc()
    {
        if (Auth::user()->id_firmy == 0) {
            return view('firma');
        } else {
            DB::update('update users set id_firmy = ? where id = ?', [0, Auth::user()->id]);
            unset($_POST);

            return redirect()->to('/firma');
        }

    }

}