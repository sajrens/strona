<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'kontrahenci_usun',
        'kontrahenci_usun/*',
        'kontrahenci_edytuj',
        'kontrahenci_edytuj/*',
        'kontrahenci_edit',
        'kontrahenci_edit/*',
        'uslugi_usun',
        'uslugi_usun/*',
        'uslugi_edytuj',
        'uslugi_edytuj/*',
        'uslugi_edit',
        'uslugi_edit/*',
        'firma_usunpracownika',
        'firma_usunpracownika/*',
        'firma_invitepracownika',
        'firma_invitepracownika/*',
        'pracownik_przyjmij',
        'pracownik_przyjmij/*',
        'faktura_nowa',
        'faktura_nowa/*',
        'faktura_usun',
        'faktura_usun/*',
        '/customers/pdf',
        '/customers/pdf/*',
    ];
}
