@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm kontra_lewa">
            <button type="button" class="btn btn-secondary" onclick="window.location.href='/kontrahenci'">Cofnij</button>
        </div>
        <div class="col-sm firma_center">
            <h1>Kontrahenci</h1>
        </div>
        <div class="col-sm kontra_prawa">

        </div>
    </div>

                        <?php
                            $kontrahent = DB::select('select * from kontrahenci WHERE id_kontrahenta = ?',[$_POST['id']]);
                            $kod='<div class="row justify-content-center"><div class="col-md-8"> <div class="card"><div class="card-header" style="background-color:#004d99; font-weight:bold; color:white;">Dodaj kontrahenta</div><div class="card-body" style="background-color:#4da6ff;"> <form method="POST" action="/kontrahenci_edit" style="color:white;" class="needs-validation" novalidate>';
                            $kod=$kod.'<div class="form-group row">'.
                                    '<label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">Imie</label>'.

                                    '<div class="col-md-6">'.
                                        '<input id="imie" type="text" class="form-control" name="imie" value="'.$kontrahent[0]->imie.'" required pattern="^[A-z]+$"><div class="invalid-feedback">
                                            Wpisz poprawne imie.
                                        </div>'.


                                   ' </div>'.

                                '</div>'.
                                '<div class="form-group row">'.
                                '<label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">Nazwisko</label>'.

                                '<div class="col-md-6">'.
                                '<input id="nazwisko" type="text" class="form-control" name="nazwisko" value="'.$kontrahent[0]->nazwisko.'" required pattern="^[A-z]+$"><div class="invalid-feedback">
                                            Wpisz poprawne nazwisko.
                                        </div>'.


                                ' </div>'.

                                '</div>'.
                                '<div class="form-group row">'.
                                '<label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">Ulica</label>'.

                                '<div class="col-md-6">'.
                                '<input id="Ulica" type="text" class="form-control" name="ulica" value="'.$kontrahent[0]->ulica.'" required>  <div class="invalid-feedback">
                                            Wpisz poprawną nazwe ulicy.
                                        </div>'.


                                ' </div>'.

                                '</div>'.
                                '<div class="form-group row">'.
                                '<label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">Lokal</label>'.

                                '<div class="col-md-6">'.
                                '<input id="lokal" type="text" class="form-control" name="lokal" value="'.$kontrahent[0]->lokal.'" required><div class="invalid-feedback">
                                            Wpisz poprawny lokal.
                                        </div>'.


                                ' </div>'.

                                '</div>'.
                                '<div class="form-group row">'.
                                '<label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">Miasto</label>'.

                                '<div class="col-md-6">'.
                                '<input id="miasto" type="text" class="form-control" name="miasto" value="'.$kontrahent[0]->miasto.'" required pattern="^[A-z]+$"><div class="invalid-feedback">
                                            Wpisz poprawną nazwe miasta.
                                        </div>'.


                                ' </div>'.

                                '</div>'.
                                '<div class="form-group row">'.
                                '<label for="kod_pocztowy" class="col-sm-4 col-form-label text-md-right" style="color:white">Kod Pocztowy</label>'.

                                '<div class="col-md-6">'.
                                '<input id="kod_pocztowy" type="text" class="form-control" name="kod_pocztowy" value="'.$kontrahent[0]->kod_pocztowy.'" required pattern="^[0-9-]+$"> <div class="invalid-feedback">
                                            Wpisz poprawny Kod pocztowy.
                                        </div>'.


                                ' </div>'.

                                '</div>'.
                                '<div class="form-group row">'.
                                '<label for="nip" class="col-sm-4 col-form-label text-md-right" style="color:white">NIP</label>'.

                                '<div class="col-md-6">'.
                                '<input id="nip" type="text" class="form-control" name="nip" value="'.$kontrahent[0]->NIP.'" required pattern="^[0-9]+$"><div class="invalid-feedback">
                                            Wpisz poprawny nip.
                                        </div>'.


                                ' </div>'.

                                '</div>'.

                                '<div class="form-group row">'.
                                '<label for="Nr_tel" class="col-sm-4 col-form-label text-md-right" style="color:white">Nr. Tel.</label>'.

                                '<div class="col-md-6">'.
                                '<input id="Nr_tel" type="text" class="form-control" name="Nr_tel" value="'.$kontrahent[0]->Nr_tel.'" required pattern="^[0-9]+$"><div class="invalid-feedback">
                                            Wpisz poprawny nr. Tel..
                                        </div>'.


                                ' </div>'.

                                '</div>'.
                                '<div class="form-group row">'.
                                '<label for="krs" class="col-sm-4 col-form-label text-md-right" style="color:white">KRS</label>'.

                                '<div class="col-md-6">'.
                                '<input id="krs" type="text" class="form-control" name="krs" value="'.$kontrahent[0]->krs.'" required pattern="^[0-9]+$"><div class="invalid-feedback">
                                            Wpisz poprawny KRS.
                                        </div>'.


                                ' </div>'.

                                '</div>'.
                                '<div class="form-group row">'.
                                '<label for="email" class="col-sm-4 col-form-label text-md-right" style="color:white">Email</label>'.

                                '<div class="col-md-6">'.
                                '<input id="email" type="text" class="form-control" name="email" value="'.$kontrahent[0]->email.'" required pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"><div class="invalid-feedback">
                                            Wpisz poprawny email.
                                        </div>'.


                                ' </div>'.

                                '</div>'.
                                '<input id="id" type="hidden" class="form-control" name="id" value="'.$_POST['id'].'" s>'.
                             '<div class="form-group row mb-0" >'.
                                    '<div class="col-md-8 offset-md-4" >'.
                                        '<button type="submit" class="btn btn-secondary" >'.
                                            'Edytuj kontrahenta'.

                                        '</button>'.


                                    '</div>'.
                                '</div>';

                            $kod=$kod.'</form></div></div></div></div>';
                            echo $kod;
                        ?>

    <script src="/js/app.js"></script>

</div>

@endsection