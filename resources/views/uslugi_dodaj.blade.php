@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm kontra_lewa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/uslugi'">Cofnij</button>
            </div>
            <div class="col-sm firma_center">
                <h1>Uslugi</h1>
            </div>
            <div class="col-sm kontra_prawa">

            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="background-color:#004d99; font-weight:bold; color:white;">Dodaj kontrahenta</div>

                    <div class="card-body" style="background-color:#4da6ff;">
                        <form method="POST" action='/uslugi_add' style="color:white;" class="needs-validation" novalidate>
                            @csrf
                            <div class="form-group row">
                                <label for="nazwa" class="col-sm-4 col-form-label text-md-right" style="color:white">Nazwa</label>

                                <div class="col-md-6">
                                    <input id="validationCustom01" type="text" class="form-control" name="nazwa" value="" required pattern="^[A-z ]+$">
                                    <div class="invalid-feedback">
                                        Wpisz poprawną nazwe.
                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">
                                <label for="cena_netto" class="col-sm-4 col-form-label text-md-right" style="color:white">Cena Netto</label>

                                <div class="col-md-6">
                                    <input id="validationCustom02" type="text" class="form-control" name="cena_netto" value="" required pattern="^[0-9.]+$">
                                    <div class="invalid-feedback">
                                        Wpisz poprawną cene.
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="vat" class="col-sm-4 col-form-label text-md-right" style="color:white">Stawka VAT</label>

                                <div class="col-md-6">
                                    <input id="validationCustom03" type="text" class="form-control" name="vat" value="" required pattern="^[0-9.]+$">
                                    <div class="invalid-feedback">
                                        Wpisz poprawy vat.
                                    </div>

                                </div>
                            </div>





                                <input type="hidden" id="_token" value="{{ csrf_token() }}">
                                <div class="form-group row mb-0" >
                                    <div class="col-md-8 offset-md-4" >
                                        <button type="submit" class="btn btn-secondary" >
                                            Dodaj usługe
                                        </button>


                                    </div>
                                </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="/js/app.js"></script>

    </div>

@endsection