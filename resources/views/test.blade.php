<html>
    <head>
        <link href='css/app.css' rel="stylesheet">
        <link href="css/pdf.css" rel="stylesheet">
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

    </head>
    <body>
        <div >

                <div style="text-align:right; font-size:10px; width: 100%">
                    <?php
                        $dzien = DB::table('faktury')->where('id_faktury', $_POST['id'])->pluck('data');
                        $vowels = array("{", "}", '"',"[","]");
                        $dzien = str_replace($vowels, '', $dzien);
                        echo "Data wystawienia: ".$dzien."<br>";
                        echo "Data sprzedaży/wykonania: ".$dzien;
                        echo "<br>usługi";
                        echo "<br>Data zakończenia: ".$dzien;


                    ?>
                </div>
        <div style="margin-top:100px; width: 100%;text-align: left; background-color: #c2c2d6;">
            FAKTURA NR:
            <?php
                echo $_POST['id'];
            ?>
        </div>
            <div style="width: 100%">
            <div style="font-size:10px; float:left;">
                <h3>Sprzedawca:</h3>
                <?php
                    $firma = DB::table('faktury')->where('id_faktury', $_POST['id'])->pluck('id_firmy');
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('nazwa');
                    $vowels = array("{", "}", '"',"[","]");
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo $dane_firmy."<br>";
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('ulica');
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo $dane_firmy;
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('lokal');
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo ' '.$dane_firmy.'<br>';
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('miasto');
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo $dane_firmy.'<br>';
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('NIP');
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo "NIP: ".$dane_firmy.'<br>';
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('Nr_tel');
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo "Nr. Tel.: ".$dane_firmy.'<br>';
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('email');
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo "Email: ".$dane_firmy.'<br>';
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('bank_name');
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo "Bank: ".$dane_firmy.'<br>';
                    $dane_firmy = DB::table('firma')->where('id_firmy', $firma)->pluck('bank_number');
                    $dane_firmy = str_replace($vowels, '', $dane_firmy);
                    echo "Nr. Konta: ".$dane_firmy.'';

                ?>

            </div>
            <div style="font-size:10px;float:right;">
                <h3>Nabywca:</h3>
                <?php
                $firma = DB::table('faktury')->where('id_faktury', $_POST['id'])->pluck('id_kontrahenta');
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('imie');
                $vowels = array("{", "}", '"',"[","]");
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo $dane_firmy." ";
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('nazwisko');
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo $dane_firmy."<br>";
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('ulica');
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo $dane_firmy;
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('lokal');
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo ' '.$dane_firmy.'<br>';
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('miasto');
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo $dane_firmy.'<br>';
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('NIP');
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo "NIP: ".$dane_firmy.'<br>';
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('Nr_tel');
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo "Nr. Tel.: ".$dane_firmy.'<br>';
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('email');
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo "Email: ".$dane_firmy.'<br>';
                $dane_firmy = DB::table('kontrahenci')->where('id_kontrahenta', $firma)->pluck('krs');
                $dane_firmy = str_replace($vowels, '', $dane_firmy);
                echo "KRS: ".$dane_firmy.'<br>';

                ?>

            </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div style="float:left; width: 100%;">
                <?php
                       $kod='<table style="width: 100%; text-align: center;">';
                            $kod=$kod.'<thead>';
                                 $kod=$kod.'<tr>'.
                                    '<th>Lp.</th>'.
                                    '<th>Nazwa usługi</th>'.
                                    '<th>Cena Netto</th>'.
                                    '<th>VAT</th>'.
                                    '<th>Cena Brutto</th>'.
                                '</tr>'.

                            '</thead>';

                                $uslugi = DB::table('faktury')->where('id_faktury', $_POST['id'])->pluck('id_uslug');
                                $uslugi = explode(",", $uslugi);
                                for($i=0;$i<count($uslugi)-1;$i++)
                                {
                                    $kod=$kod."<tr><th>".($i+1)."</th>";
                                    $vowels = array("{", "}", '"',"[","]");
                                    $uslugi[$i] = str_replace($vowels, '', $uslugi[$i]);
                                    $nazwa = DB::table('uslugi')->where('id_uslugi', $uslugi[$i])->pluck('nazwa');
                                     $vowels = array("{", "}", '"',"[","]");
                                     $nazwa = str_replace($vowels, '', $nazwa);
                                     $kod=$kod."<th>".$nazwa."</th>";
                                        $nazwa = DB::table('uslugi')->where('id_uslugi', $uslugi[$i])->pluck('cena_netto');
                                        $vowels = array("{", "}", '"',"[","]");
                                        $nazwa = str_replace($vowels, '', $nazwa);
                                        $kod=$kod."<th>".$nazwa."</th>";
                                    $kod=$kod."<th>23%</th>";
                                    $kod=$kod."<th>".(intval($nazwa)*1.23)."</th>";

                                     $kod=$kod."</tr>";
                                }



                        $kod=$kod.'</table>';
                                echo $kod;
                ?>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div style="width: 200px;text-align: right;">
                <?php
                    $kod='<table style="width: 30%; text-align: center;">';
                    $kod=$kod."<thead>";
                    $kod=$kod.'<tr>'.
                        '<th >Stawka Vat</th>'.
                        '<th>Netto</th>'.
                        '<th>VAT</th>'.
                        '<th>Brutto</th>'.
                        '</tr>';
                    $kod=$kod."</thead>";


                    $uslugi = DB::table('faktury')->where('id_faktury', $_POST['id'])->pluck('id_uslug');
                    $vowels = array("{", "}", '"',"[","]");
                    $uslugi = explode(",", $uslugi);
                $uslugi[0] = str_replace($vowels, '', $uslugi[0]);
                    $cena = 0;
                    for($i=0;$i<count($uslugi);$i++)
                    {
                        $cena_uslugi = DB::table('uslugi')->where('id_uslugi', $uslugi[$i])->pluck('cena_netto');
                        $cena_uslugi = str_replace($vowels, '', $cena_uslugi);
                        $cena=$cena+intval($cena_uslugi);
                    }
                    $kod=$kod."<tr><th>23%</th>";
                    $kod=$kod."<th>".$cena."</th>";
                    $kod=$kod."<th>".($cena*0.23)."</th>";
                    $kod=$kod."<th>".($cena*1.23)."</th></tr>";
                    $kod=$kod."</table>";
                    $kod=$kod."<br>".
                "Sposób zapłaty: przelew<br>".
                    "Termin zapłaty: zapłacono".
                    "<br>".
                "Dostawa: Nie dotyczy";
                echo $kod;
                ?>

            </div>
            <br><br><br><br>
            <div style="float:left; margin-left: 20px;">
Podpis<br>Osoba Odbierajaca
            </div>
            <div style="float:right;margin-right: 20px;">
Podpis<br>Osoba Wystawiająca
            </div>




        </div>
    </body>
</html>