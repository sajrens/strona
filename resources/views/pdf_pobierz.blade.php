@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm kontra_lewa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/home'">Cofnij</button>
            </div>
            <div class="col-sm firma_center">
                <h1>Faktury</h1>
            </div>
            <div class="col-sm kontra_prawa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/faktura_dodaj'">Dodaj</button>
            </div>
        </div>
        <div class="row" style="margin-top:10px;">

            <?php
            $faktury = DB::select('select * from faktury WHERE id_firmy = ?',[Auth::user()->id_firmy]);
            $kod = '<table class="table" id="myTable"> <thead><tr><th scope="col">Usuń</th><th scope="col">PDF</th><th scope="col">Kontrahent</th><th scope="col">Cena(z vat)</th><th scope="col">Data</th></thead>';
            foreach ($faktury as $faktura) {
                $kod=$kod."<tr>";
                $kod=$kod ."<td><form action='/faktura_usun' method='POST'> <?php echo @csrf; ?><input type='hidden' id='_token' value='".  csrf_token()  ."'><input type='hidden' value='".$faktura->id_faktury."' name='id'><button class='btn btn-secondary' type='submit'>Usuń</button></form></td>";
                $kod=$kod ."<td><form action='/customers/pdf' method='POST'> <?php echo @csrf; ?><input type='hidden' id='_token' value='".  csrf_token()  ."'><input type='hidden' value='".$faktura->id_faktury."' name='id'><button class='btn btn-secondary' type='submit'>PDF</button></form></td>";
                $kontrahent = DB::table('kontrahenci')->where('id_kontrahenta', $faktura->id_kontrahenta)->pluck('imie','nazwisko');
                $vowels = array("{", "}", '"',"[","]");
                $kontrahent = str_replace($vowels, '', $kontrahent);
                $kontrahent = str_replace(':', ' ', $kontrahent);
                $kod=$kod ."<td>". $kontrahent."</td>";
                $uslugi = explode(",", $faktura->id_uslug);
                $cena = 0;
                for($i=0;$i<count($uslugi);$i++)
                    {
                        $cena_uslugi = DB::table('uslugi')->where('id_uslugi', $uslugi[$i])->pluck('cena_netto');
                        $cena_uslugi = str_replace($vowels, '', $cena_uslugi);
                        $cena=$cena+intval($cena_uslugi);
                    }
                $kod=$kod ."<td>". $cena*1.23." zł</td>";
                $kod=$kod ."<td>". $faktura->data."</td>";

                $kod=$kod."</tr>";
            }
            $kod = $kod. "</table>";

            echo $kod;


            ?>
            @csrf
        </div>

    </div>
@endsection