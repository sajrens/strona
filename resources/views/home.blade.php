@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row tak">
            <div class="col-sm klocki" onclick="javascript:window.location.href='/kontrahenci'">

                <i class="fas fa-people-carry fa-9x granica"></i>
                    <p class="bialo">Kontrahenci</p>

            </div>
            <div class="col-sm klocki dalej" onclick="javascript:window.location.href='/uslugi'">
                <i class="fas fa-hammer fa-9x granica"></i>
                <p class="bialo">Usługi</p>
            </div>
            <div class="col-sm klocki dalej">
                <i class="far fa-plus-square fa-9x granica" onclick="javascript:window.location.href='/faktura_dodaj'"></i>
                <p class="bialo">Dodaj Fakture</p>
            </div>
            <div class="col-sm klocki dalej">
                <i class="fas fa-print fa-9x granica" onclick="javascript:window.location.href='/pdf_pobierz'"></i>
                <p class="bialo">Pobierz pdf</p>
            </div>
            <div class="col-sm klocki dalej">
                <i class="fas fa-users fa-9x granica" onclick="javascript:window.location.href='/firma'"></i>
                <p class="bialo">Firma</p>
            </div>
        </div>


    </div>
@endsection
