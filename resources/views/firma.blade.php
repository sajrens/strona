@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="alert alert-primary firma_alert" role="alert">
                    Nie masz jeszcze firmy, dołącz do istniejącej lub dodaj swoją!
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm firma_center">
                <button type="button" class="btn btn-secondary btn-lg" onclick="javascript:window.location.href='/pracownik_zaproszenia'">Zobacz zaproszenia</button>
                <button type="button" class="btn btn-secondary btn-lg" onclick="javascript:window.location.href='/home'">Wróć na strone główną</button>
                <button type="button" class="btn btn-secondary btn-lg" onclick="javascript:window.location.href='/firma_create'">Załóż swoją firme</button>
            </div>
        </div>


    </div>
@endsection