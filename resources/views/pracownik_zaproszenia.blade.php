@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-sm kontra_lewa">
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='/firma'">Cofnij</button>
                </div>
                <div class="col-sm firma_center">
                    <h1>Zaproszenia</h1>
                </div>
                <div class="col-sm kontra_prawa">

                </div>
            </div>
            <div class="row" style="margin-top:10px;">

                <?php
                $zaproszenia = DB::select('SELECT firma.nazwa,firma.email,firma.id_firmy FROM firma INNER JOIN zaproszenia ON firma.id_firmy = zaproszenia.id_firmy WHERE zaproszenia.id_user=?',[Auth::user()->id]);
                $kod = '<table class="table" id="myTable"> <thead><tr><th scope="col">Przyjmij zaproszenie</th><th scope="col">Nazwa Firmy</th></thead>';
                foreach ($zaproszenia as $zaproszenia) {
                    $kod=$kod."<tr>";
                    $kod=$kod ."<td><form action='/pracownik_przyjmij' method='POST'> <?php echo @csrf; ?><input type='hidden' id='_token' value='".  csrf_token()  ."'><input type='hidden' name='firma' value='".$zaproszenia->id_firmy."'><input type='hidden' value='".Auth::user()->id."' name='id'><button class='btn btn-secondary' type='submit'>Przyjmij zaproszenie</button></form></td>";
                    $kod=$kod ."<td>". $zaproszenia->nazwa."</td>";
                    $kod=$kod ."<td>". $zaproszenia->email."</td>";

                    $kod=$kod."</tr>";
                }
                $kod = $kod. "</table>";

                echo $kod;


                ?>
                @csrf
            </div>

        </div>
    </div>
@endsection