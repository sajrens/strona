@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm kontra_lewa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/uslugi'">Cofnij</button>
            </div>
            <div class="col-sm firma_center">
                <h1>Usługi</h1>
            </div>
            <div class="col-sm kontra_prawa">

            </div>
        </div>

        <?php
        $uslugi = DB::select('select * from uslugi WHERE id_uslugi = ?',[$_POST['id']]);
        $kod='<div class="row justify-content-center"><div class="col-md-8"> <div class="card"><div class="card-header" style="background-color:#004d99; font-weight:bold; color:white;">Dodaj kontrahenta</div><div class="card-body" style="background-color:#4da6ff;"> <form method="POST" action="/uslugi_edit" style="color:white;" class="needs-validation" novalidate>';
        $kod=$kod.'<div class="form-group row">'.
            '<label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">Nazwa</label>'.

            '<div class="col-md-6">'.
            '<input id="imie" type="text" class="form-control" name="nazwa" value="'.$uslugi[0]->nazwa.'" required pattern="^[A-z ]+$">'.


            ' </div>'.

            '</div>'.
            '<div class="form-group row">'.
            '<label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">Cena Netto</label>'.

            '<div class="col-md-6">'.
            '<input id="nazwisko" type="text" class="form-control" name="cena" value="'.$uslugi[0]->cena_netto.'" required pattern="^[0-9.]+$">'.


            ' </div>'.

            '</div>'.
            '<div class="form-group row">'.
            '<label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">VAT</label>'.

            '<div class="col-md-6">'.
            '<input id="Ulica" type="text" class="form-control" name="vat" value="'.$uslugi[0]->vat.'" required pattern="^[0-9.]+$">'.


            ' </div>'.

            '</div>'.
            '<input id="id" type="hidden" class="form-control" name="id" value="'.$_POST['id'].'" s>'.
            '<div class="form-group row mb-0" >'.
            '<div class="col-md-8 offset-md-4" >'.
            '<button type="submit" class="btn btn-secondary" >'.
            'Edytuj usługe'.

            '</button>'.


            '</div>'.
            '</div>';

        $kod=$kod.'</form></div></div></div></div>';
        echo $kod;
        ?>

        <script src="/js/app.js"></script>

    </div>

@endsection