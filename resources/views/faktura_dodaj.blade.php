@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm kontra_lewa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/home'">Cofnij</button>
            </div>
            <div class="col-sm firma_center">
                <h1>Faktura</h1>
            </div>
            <div class="col-sm kontra_prawa">

            </div>
        </div>

            <?php

            $kod='<div class="row justify-content-center"><div class="col-md-8"> <div class="card"><div class="card-header" style="background-color:#004d99; font-weight:bold; color:white;">Dodaj kontrahenta</div><div class="card-body" style="background-color:#4da6ff;"> <form method="POST" action="/faktura_nowa" style="color:white;" class="needs-validation" novalidate>';
            $kod=$kod.'<div class="form-group row" style="text-align: center;">Kontrahent:'.
                '</div>'.
                '<div class="form-group row" id="kontrahent">'.

                '<select class="form-control" name="kontrahent" required>'.
                '<option>Wybierz kontrahenta</option>';
                $kontrahenci = DB::select('select * from kontrahenci WHERE id_firmy = ?',[Auth::user()->id_firmy]);
                foreach ($kontrahenci as $kontrahent) {
                    $kod=$kod.'<option>'.$kontrahent->id_kontrahenta.' | '.$kontrahent->imie.' | '.$kontrahent->nazwisko.' | '.$kontrahent->email.'</option>';
                }

                $kod=$kod.'</select><button type="button" class="btn btn-secondary granica" onclick="faktura_kontrahent()">Inny kontrahent</button>'.

                '</div>';
                $kod=$kod.'<div class="form-group row" style="text-align: center;">Usługi:'.
                    '</div>'.
                    '<div id="usluga">'.
                    '<div class="form-group row" id="usluga1">'.

                    '<select class="form-control" onchange="nowy_select()" name="usluga1" id="uslugi" required>'.
                    '<option>Wybierz usługe</option>';
                     $uslugi = DB::select('select * from uslugi WHERE id_firmy = ?',[Auth::user()->id_firmy]);
                    foreach ($uslugi as $usluga) {
                        $kod=$kod.'<option>'.$usluga->id_uslugi.' | '.$usluga->nazwa.' | '.$usluga->cena_netto.'zł | '.$usluga->vat.'%</option>';
                    }

                    $kod=$kod.'</select>'.

                        '</div>'.
                    '</div>';



                $kod=$kod.'<input id="ilosc" type="hidden" class="form-control" name="ilosc_uslug" value="1">'.
                '<div class="form-group row mb-0" >'.
                '<div class="col-md-8 offset-md-4" >'.
                '<button type="submit" class="btn btn-secondary" >'.
                'Dodaj fakture'.

                '</button>'.


                '</div>'.
                '</div>';

            $kod=$kod.'</form></div></div></div></div>';
            echo $kod;
            ?>



        <script src="/js/app.js"></script>



    </div>

@endsection
