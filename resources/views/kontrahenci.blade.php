@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm kontra_lewa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/home'">Cofnij</button>
            </div>
            <div class="col-sm firma_center">
                <h1>Kontrahenci</h1>
            </div>
            <div class="col-sm kontra_prawa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/kontrahenci_dodaj'">Dodaj</button>
            </div>
        </div>
        <div class="row" style="margin-top:10px;">

            <?php
            $kontrahents = DB::select('select * from kontrahenci WHERE id_firmy = ?',[Auth::user()->id_firmy]);
            $kod = '<table class="table" id="myTable"> <thead><tr><th scope="col">Edytuj</th><th scope="col">Usuń</th><th scope="col">Imie</th><th scope="col">Nazwisko</th><th scope="col">Ulica</th> <th scope="col">Lokal</th><th scope="col">Miasto</th><th scope="col">Kod Pocztowy</th><th scope="col">NIP</th><th scope="col">Nr. Tel.</th><th scope="col">KRS</th><th scope="col">Email</th></thead>';
            foreach ($kontrahents as $kontrahent) {
                $kod=$kod."<tr>";
                $kod=$kod ."<td><form action='/kontrahenci_edytuj' method='POST'> <?php echo @csrf; ?><input type='hidden' id='_token' value='".  csrf_token()  ."'><input type='hidden' value='".$kontrahent->id_kontrahenta."' name='id'><button class='btn btn-secondary' type='submit'>Edytuj</button></form></td>";
                $kod=$kod ."<td><form action='/kontrahenci_usun' method='POST'> <?php echo @csrf; ?><input type='hidden' id='_token' value='".  csrf_token()  ."'><input type='hidden' value='".$kontrahent->id_kontrahenta."' name='id'><button class='btn btn-secondary' type='submit'>Usuń</button></form></td>";
                $kod=$kod ."<td>". $kontrahent->imie."</td>";
                $kod=$kod ."<td>". $kontrahent->nazwisko."</td>";
                $kod=$kod ."<td>". $kontrahent->ulica."</td>";
                $kod=$kod ."<td>". $kontrahent->lokal."</td>";
                $kod=$kod ."<td>". $kontrahent->miasto."</td>";
                $kod=$kod ."<td>". $kontrahent->kod_pocztowy."</td>";
                $kod=$kod ."<td>". $kontrahent->NIP."</td>";
                $kod=$kod ."<td>". $kontrahent->Nr_tel."</td>";
                $kod=$kod ."<td>". $kontrahent->krs."</td>";
                $kod=$kod ."<td>". $kontrahent->email."</td>";
                $kod=$kod."</tr>";
            }
            $kod = $kod. "</table>";

            echo $kod;


            ?>
            @csrf
        </div>

    </div>
@endsection