@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="background-color:#004d99; font-weight:bold; color:white;">Stworz firme</div>

                    <div class="card-body" style="background-color:#4da6ff;">
                        <form method="POST" action='/firma_done' style="color:white;">
                            @csrf
                            <div class="form-group row">
                                <label for="nazwa" class="col-sm-4 col-form-label text-md-right" style="color:white">Nazwa</label>

                                <div class="col-md-6">
                                    <input id="nazwa" type="text" class="form-control" name="nazwa" value="" required pattern="^[A-z ]+$">
                                    <div class="invalid-feedback">
                                        Wpisz poprawną nazwe.
                                    </div>


                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ulica" class="col-sm-4 col-form-label text-md-right" style="color:white">Ulica</label>

                                <div class="col-md-6">
                                    <input id="" type="text" class="form-control" name="ulica" value="" required>
                                    <div class="invalid-feedback">
                                        Wpisz poprawną nazwe ulicy.
                                    </div>


                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lokal" class="col-sm-4 col-form-label text-md-right" style="color:white">Nr lokalu</label>

                                <div class="col-md-6">
                                    <input id="" type="text" class="form-control" name="lokal" value="" required>
                                    <div class="invalid-feedback">
                                        Wpisz poprawny lokal.
                                    </div>


                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="miasto" class="col-sm-4 col-form-label text-md-right" style="color:white">Miasto</label>

                                <div class="col-md-6">
                                    <input id="" type="text" class="form-control" name="miasto" value="" required pattern="^[A-z]+$">
                                    <div class="invalid-feedback">
                                        Wpisz poprawną nazwe miasta.
                                    </div>


                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="NIP" class="col-sm-4 col-form-label text-md-right" style="color:white">NIP</label>

                                <div class="col-md-6">
                                    <input id="" type="text" class="form-control" name="nip" value="" required pattern="^[0-9]+$">
                                    <div class="invalid-feedback">
                                        Wpisz poprawny NIP.
                                    </div>


                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telefon" class="col-sm-4 col-form-label text-md-right" style="color:white">Nr. Tel.</label>

                                <div class="col-md-6">
                                    <input id="" type="text" class="form-control" name="telefon" value="" required pattern="^[0-9]+$" >
                                    <div class="invalid-feedback">
                                        Wpisz poprawny Nr. Tel.
                                    </div>


                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right" style="color:white">Email</label>

                                <div class="col-md-6">
                                    <input id="" type="email" class="form-control" name="email" value="" required pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$">
                                    <div class="invalid-feedback">
                                        Wpisz poprawny email.
                                    </div>



                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bank_name" class="col-sm-4 col-form-label text-md-right" style="color:white">Nazwa Banku</label>

                                <div class="col-md-6">
                                    <input id="" type="text" class="form-control" name="bank_name" value="" required>
                                    <div class="invalid-feedback">
                                        To pole jest wymagane.
                                    </div>



                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bank_nr" class="col-sm-4 col-form-label text-md-right" style="color:white">Numer konta bankowego</label>

                                <div class="col-md-6">
                                    <input id="" type="text" class="form-control" name="bank_nr" value="" required pattern="^[0-9]+$">
                                    <div class="invalid-feedback">
                                        Wpisz poprawny nr.
                                    </div>


                                </div>
                            </div>
                            <input type="hidden" id="_token" value="{{ csrf_token() }}">
                            <div class="form-group row mb-0" >
                                <div class="col-md-8 offset-md-4" >
                                    <button type="submit" class="btn btn-secondary" >
                                        Stwórz firme
                                    </button>


                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="/js/app.js"></script>

    </div>
@endsection