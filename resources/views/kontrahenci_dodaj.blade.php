@extends('layouts.app')

@section('content')
        <div class="container">
            <div class="row">
                <div class="col-sm kontra_lewa">
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='/kontrahenci'">Cofnij</button>
                </div>
                <div class="col-sm firma_center">
                    <h1>Kontrahenci</h1>
                </div>
                <div class="col-sm kontra_prawa">

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color:#004d99; font-weight:bold; color:white;">Dodaj kontrahenta</div>

                        <div class="card-body" style="background-color:#4da6ff;">
                            <form method="POST" action='/kontrahenci_add' style="color:white;" class="needs-validation" novalidate>
                                @csrf
                                <div class="form-group row">
                                    <label for="imie" class="col-sm-4 col-form-label text-md-right" style="color:white">Imie</label>

                                    <div class="col-md-6">
                                        <input id="imie" type="text" class="form-control" name="imie" value="" required pattern="^[A-z]+$">
                                        <div class="invalid-feedback">
                                            Wpisz poprawne imie.
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nazwisko" class="col-sm-4 col-form-label text-md-right" style="color:white">Nazwisko</label>

                                    <div class="col-md-6">
                                        <input id="nazwisko" type="text" class="form-control" name="nazwisko" value="" required pattern="^[A-z]+$">
                                        <div class="invalid-feedback">
                                            Wpisz poprawne nazwisko.
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ulica" class="col-sm-4 col-form-label text-md-right" style="color:white">Ulica</label>

                                    <div class="col-md-6">
                                        <input id="" type="text" class="form-control" name="ulica" value="" required>
                                        <div class="invalid-feedback">
                                            Wpisz poprawną nazwe ulicy.
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="miasto" class="col-sm-4 col-form-label text-md-right" style="color:white">Lokal</label>

                                    <div class="col-md-6">
                                        <input id="" type="text" class="form-control" name="lokal" value="" required>
                                        <div class="invalid-feedback">
                                            Wpisz poprawny lokal.
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="NIP" class="col-sm-4 col-form-label text-md-right" style="color:white">Miasto</label>

                                    <div class="col-md-6">
                                        <input id="" type="text" class="form-control" name="miasto" value="" required pattern="^[A-z]+$">
                                        <div class="invalid-feedback">
                                            Wpisz poprawną nazwe miasta.
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="telefon" class="col-sm-4 col-form-label text-md-right" style="color:white">Kod Pocztowy</label>

                                    <div class="col-md-6">
                                        <input id="" type="text" class="form-control" name="kod_pocztowy" value="" required pattern="^[0-9-]+$">
                                        <div class="invalid-feedback">
                                            Wpisz poprawny Kod pocztowy.
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right" style="color:white">NIP</label>

                                    <div class="col-md-6">
                                        <input id="" type="text" class="form-control" name="nip" value="" required pattern="^[0-9]+$">
                                        <div class="invalid-feedback">
                                            Wpisz poprawny nip.
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bank_name" class="col-sm-4 col-form-label text-md-right" style="color:white">Nr. Tel.</label>

                                    <div class="col-md-6">
                                        <input id="" type="text" class="form-control" name="nr_tel" value="" required pattern="^[0-9]+$">
                                        <div class="invalid-feedback">
                                            Wpisz poprawny nr. Tel..
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bank_nr" class="col-sm-4 col-form-label text-md-right" style="color:white">KRS</label>

                                    <div class="col-md-6">
                                        <input id="" type="text" class="form-control" name="krs" value="" required pattern="^[0-9]+$">
                                        <div class="invalid-feedback">
                                            Wpisz poprawny KRS.
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right" style="color:white">Email</label>

                                    <div class="col-md-6">
                                        <input id="" type="email" class="form-control" name="email" value="" required pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$">
                                        <div class="invalid-feedback">
                                            Wpisz poprawny email.
                                        </div>


                                    </div>
                                <input type="hidden" id="_token" value="{{ csrf_token() }}">
                                <div class="form-group row mb-0" >
                                    <div class="col-md-8 offset-md-4" >
                                        <button type="submit" class="btn btn-secondary" >
                                            Dodaj kontrahenta
                                        </button>


                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script src="/js/app.js"></script>

        </div>

@endsection