@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-sm kontra_lewa">
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='/firma'">Cofnij</button>
                </div>
                <div class="col-sm firma_center">
                    <h1>Zaproś pracownika</h1>
                </div>
                <div class="col-sm kontra_prawa">

                </div>
            </div>
            <div class="row" style="margin-top:10px;">

                <?php

                $users = DB::select('SELECT * FROM users WHERE id_firmy = 0 AND NOT id IN (SELECT id_user FROM zaproszenia WHERE id_firmy = ? )',[Auth::user()->id_firmy]);

                $kod = '<table class="table" id="myTable"> <thead><tr><th scope="col">Zaproś</th><th scope="col">Nazwa</th><th scope="col">Email</th></thead>';
                foreach ($users as $user) {
                    $kod=$kod."<tr>";
                    $kod=$kod ."<td><form action='/firma_invitepracownika' method='POST'> <?php echo @csrf; ?><input type='hidden' id='_token' value='".  csrf_token()  ."'><input type='hidden' value='".$user->id."' name='id'><button class='btn btn-secondary' type='submit'>Zaproś</button></form></td>";
                    $kod=$kod ."<td>". $user->name."</td>";
                    $kod=$kod ."<td>". $user->email."</td>";

                    $kod=$kod."</tr>";
                }
                $kod = $kod. "</table>";

                echo $kod;


                ?>
                @csrf
            </div>

        </div>
    </div>
@endsection