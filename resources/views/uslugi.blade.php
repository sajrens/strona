@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm kontra_lewa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/home'">Cofnij</button>
            </div>
            <div class="col-sm firma_center">
                <h1>Uslugi</h1>
            </div>
            <div class="col-sm kontra_prawa">
                <button type="button" class="btn btn-secondary" onclick="window.location.href='/uslugi_dodaj'">Dodaj</button>
            </div>
        </div>
        <div class="row" style="margin-top:10px;">

                <?php
                    $uslugi = DB::select('select * from uslugi WHERE id_firmy = ?',[Auth::user()->id_firmy]);
                    $kod = '<table class="table" id="myTable"> <thead><tr><th scope="col">Edytuj</th><th scope="col">Usuń</th><th scope="col">Nazwa</th><th scope="col">Cena netto</th><th scope="col">Stawka Vat</th></thead>';
                    foreach ($uslugi as $usluga) {
                        $kod=$kod."<tr>";
                        $kod=$kod ."<td><form action='/uslugi_edytuj' method='POST'> <?php echo @csrf; ?><input type='hidden' id='_token' value='".  csrf_token()  ."'><input type='hidden' value='".$usluga->id_uslugi."' name='id'><button class='btn btn-secondary' type='submit'>Edytuj</button></form></td>";
                        $kod=$kod ."<td><form action='/uslugi_usun' method='POST'> <?php echo @csrf; ?><input type='hidden' id='_token' value='".  csrf_token()  ."'><input type='hidden' value='".$usluga->id_uslugi."' name='id'><button class='btn btn-secondary' type='submit'>Usuń</button></form></td>";
                       $kod=$kod ."<td>". $usluga->nazwa."</td>";
                        $kod=$kod ."<td>". $usluga->cena_netto."</td>";
                        $kod=$kod ."<td>". $usluga->vat."</td>";

                        $kod=$kod."</tr>";
                    }
                    $kod = $kod. "</table>";

                    echo $kod;


                ?>
                    @csrf
        </div>

    </div>
@endsection