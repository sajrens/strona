<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Uslugi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uslugi', function (Blueprint $table) {
            $table->integer('id_firmy');
            $table->increments('id_uslugi')->unique();
            $table->string('nazwa');
            $table->double('cena_netto', 8, 0);
            $table->integer('vat');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uslugi');
    }
}
