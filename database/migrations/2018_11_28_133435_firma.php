<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Firma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firma', function (Blueprint $table) {
            $table->increments('id_firmy');
            $table->integer('id_owner');
            $table->string('nazwa');
            $table->string('ulica');
            $table->string('lokal');
            $table->string('miasto');
            $table->string('NIP');
            $table->string('Nr_tel');
            $table->string('email')->unique();
            $table->string('bank_name');
            $table->integer('bank_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('firma');
    }
}
