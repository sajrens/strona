<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kontrahenci extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontrahenci', function (Blueprint $table) {
            $table->integer('id_firmy');
            $table->increments('id_kontrahenta');
            $table->string('imie');
            $table->string('nazwisko');
            $table->string('ulica');
            $table->string('lokal');
            $table->string('miasto');
            $table->string('kod_pocztowy');
            $table->string('NIP');
            $table->string('Nr_tel');
            $table->string('krs');
            $table->string('email')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontrahenci');
    }
}
