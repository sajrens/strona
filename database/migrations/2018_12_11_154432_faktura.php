<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Faktura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faktury', function (Blueprint $table) {
            $table->increments('id_faktury');
            $table->integer('id_firmy');
            $table->integer('id_kontrahenta');
            $table->string('id_uslug');
            $table->string('data');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faktury');
    }
}
