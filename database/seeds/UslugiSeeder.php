<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UslugiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            DB::table('uslugi')->insert([
                'id_firmy' => 1,
                'nazwa' => $faker->company,
                'cena_netto' => $faker->numberBetween(1, 100),
                'vat' => $faker->numberBetween(0, 23)

            ]);
        }
    }
}
