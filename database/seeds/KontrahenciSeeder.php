<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class KontrahenciSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            DB::table('kontrahenci')->insert([
                'id_firmy' => 1,
                'imie' => $faker->name,
                'nazwisko' => $faker->lastName,
                'ulica' => $faker->word,
                'lokal' => $faker->numberBetween(1, 100),
                'miasto' => $faker->city,
                'kod_pocztowy' => $faker->numberBetween(1, 100),
                'NIP' => $faker->numberBetween(1, 100),
                'Nr_tel' => $faker->numberBetween(1, 100),
                'krs' => $faker->numberBetween(1, 100),
                'email' => $faker->email,
            ]);
        }
    }
}
